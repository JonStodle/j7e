<?php

use App\Airtable;

Route::get('/list', function() {
    $records = Airtable::getInstance()->getPaths();
    return view('list', compact('records'));
});

Route::get('/{slug?}', function($slug = null) {
    if (!$slug)
    {
        return redirect(env('DEFAULT_REDIRECT'), 301);
    }

    $records = Airtable::getInstance()->getPaths('LOWER({Path}) = LOWER("' . $slug . '")');

    if(count($records) < 1)
    {
        abort(404);
    }

    return redirect(array_values($records)[0]->fields->Url, 301);
});
