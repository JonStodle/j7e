<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>j7e</title>

    <style>
        body{
            font-family: sans-serif;
        }

        table{
            margin: 0 auto;
            border-collapse: collapse;
        }

        td{
            padding: 1rem;
        }

        thead{
            font-weight: bold;
        }

        tr{
            border-bottom: 1px solid black;
        }
    </style>
</head>
<body>
<table>
    <thead>
    <tr>
        <td>Slug</td>
        <td>URL</td>
        <td>Description</td>
    </tr>
    </thead>
    <tbody>
        @foreach($records as $record)
            <tr>
                <td><a href="{{ "https://j7e.no/{$record->fields->Path}" }}">{{ $record->fields->Path }}</a></td>
                <td><a href="{{ $record->fields->Url }}">{{ $record->fields->Url }}</a></td>
                <td>{{ property_exists($record->fields, 'Description') ? $record->fields->Description : '' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>