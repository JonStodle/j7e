<?php

namespace App;


class Airtable
{
    private static $_instance = null;
    public static function getInstance()
    {
        if (is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->_baseUrl = 'https://api.airtable.com/v0/appWl2zMBzmuB0zOA/Paths';

        $this->_curl = curl_init();
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . env('AIRTABLE_API_KEY')
        ));
    }

    public function getPaths($filter = null)
    {
        $url = $this->_baseUrl;

        if (!is_null($filter))
        {
            $url = $url . '?filterByFormula=' . urlencode($filter);
        }

        curl_setopt($this->_curl, CURLOPT_URL, $url);
        $response = json_decode(curl_exec($this->_curl));

        if (property_exists($response, 'records'))
        {
            return $response->records;
        }
        else
        {
            return array();
        }
    }

    private $_baseUrl;
    private $_curl;
}